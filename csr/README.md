# Node Boiler Plate
The motivation behind this boilerplate was to not lay ourselves in submission to any framework's ways. Rather building on top of the most unopinionated framework (Expressjs) in existance for Nodejs. 
By building custom boilerplate without the logic hiding behind abstractions of a library or an npm package we enable ourselves to have utmost control over the way our code flows. 

This boilerplate is simple ol' Expressjs which is preconfigured to help us develop rapidly and without compromising control. If you dont like the way a thing is done you can simply go in there and tinker with the preconfigured settings to get the outcome that most suits your workflow and development needs.



## Table of Contents

- [Features](#features)
- [Commands](#commands)
- [Project Structure](#project-structure)
- [Project Files](#project-files)
- [API Documentation](#api-documentation)
- [Error Handling](#error-handling)
- [Validation](#validation)
- [Authentication](#authentication)
- [Authorization](#authorization)
- [Logging](#logging)
- [Custom Mongoose Plugins](#custom-mongoose-plugins)


## Features
- **JavaScript**: Latest ECMAScript features
- **NoSQL database**: [MongoDB](https://www.mongodb.com) using [Mongoose](https://mongoosejs.com) as an ODM
- **Authentication**: User Authentication using [passport](http://www.passportjs.org)
- **Authorization**: Custom RBAC implementation
- **Validation**: Request data validation using [Joi](https://github.com/hapijs/joi) with custom middlewares
- **Logging**: [winston](https://github.com/winstonjs/winston) and [morgan](https://github.com/expressjs/morgan) to handle system level logging
- **Testing**: Unit and integration tests using [Jest](https://jestjs.io)
- **Error Handling**: Centralized custom error handling mechanism
- **API Documentation**: Documentation with [swagger-jsdoc](https://github.com/Surnet/swagger-jsdoc) and [swagger-ui-express](https://github.com/scottie1984/swagger-ui-express)
- **Process Management**: Advanced production process management using [PM2](https://pm2.keymetrics.io)
- **Dependency Management**: with [Yarn](https://yarnpkg.com)
- **Environment Variables**: using [dotenv](https://github.com/motdotla/dotenv) and [cross-env](https://github.com/kentcdodds/cross-env#readme)
- **Security**: set security HTTP headers using [helmet](https://helmetjs.github.io)
- **Santizing**: sanitize request data against xss and query injection
- **CORS**: Cross-Origin Resource-Sharing enabled using [cors](https://github.com/expressjs/cors)
- **Compression**: gzip compression with [compression](https://github.com/expressjs/compression)
- **CI**: continuous integration with [Travis CI](https://travis-ci.org)
- **SMTP Mailing Server**: setup using [Nodemailer](https://nodemailer.com/about/)


## Commands

Running locally:

```bash
yarn dev
```
or
```bash
npm run dev
```

Running in production:

```bash
yarn start
```
or
```bash
npm run start
```


## Project Structure 
```
src\
 |--config\         # Environment variables and configuration of external packages
 |--controllers\    # Route controllers
 |--docs\           # Swagger files
 |--middlewares\    # Custom express middlewares
 |--models\         # Mongoose models
 |--routes\         # Routes
 |--services\       # Database queries 
 |--utils\          # Utility classes and functions
 |--validations\    # Request data validation schemas
 |--app.js          # Express app
 |--index.js        # App entry point
```

## Project Files

- **.dockerignore**: 
Allows you to exclude files from docker image upon build process initiation 

- **.editorconfig**:
An open source file format that helps you configure and enforce formatting/code style conventions to achieve consistent, more readable codebases. EditorConfig files are applied at repository and project levels.

- **.env.example**:
Example of what the .env file looks like with dummy values for the keys required.

- **.eslintignore**:
Plain text file allowing us to omit paths from linting.

- **.eslintrc.json**:
ESLint's Configuration file.

- **.gitattributes**:
Tells git to modify how it performs certain operations (if/when it performs said operation) on files at a given path.

- **.gitignore**:
Tells git that by default it shouldn't pay attention to untracked files at a given path. 
In simple words allows you to exclude files from your git repository.

- **.lintstagedrc.json**:
File containing an object used to search for staged files that match the micromatch pattern in its key to run an array of commands against said files.

- **.prettierignore**:
Ignore aka not reformat certain files and folders completely.

- **.prettierrc.json**:
Prettier's Configuration file.

- **.travis.yml**:
YAML file at the root directory of the repository specifying the programming language used, the desired building and testing environment (including dependencies which must be installed before the software can be built and tested), etc.

- **docker-compose.dev.yml**:
YAML file to configure services for development environment that make up your application. This file acts as an attachment to the docker-compose.yml file.

- **docker-compose.prod.yml**:
YAML file to configure services for Production environment that make up your application. This file acts as an attachment to the docker-compose.yml file.

- **docker-compose.test.yml**:
YAML file to configure services for Testing environment that make up your application. This file acts as an attachment to the docker-compose.yml file.

- **docker-compose.yml**:
YAML file to configure services that make up your application. Docker Compose is a tool for defining and running multi-container Docker applications. We add the base configuration here which is then attached to a environment level file for modular approach for different environments

- **Dockerfile**:
A text file containing all the commands a user could call on the command line to assemble an image .You can create an automated build that executes several command-line instructions in succession using docker build.

- **ecosystem.config.json**:
A javascript file exporting a configuration object used to gather all options and environment variables for all your applications. 
The object has two properties: 
-- apps, an array that contains the configuration for each process.
-- deploy, an object that contains the configuration for the deployments

- **jest.config.js**:
Jest's configuration file.

- **README.md**:
The Holy Grail of this boilerplate that one must read in order to find the meaning of existence, Node's existance, in tandem with other technologies :D


As Always Happy Coding!
Hope it helps.